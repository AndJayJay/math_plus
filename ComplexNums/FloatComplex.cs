﻿using UnityEngine;

namespace MathPlus.ComplexNums
{
    public struct ComplexFloat
    {
        float mod;
        float arg;

        public float Mod
        {
            get
            {
                return mod;
            }
            set
            {
                if (value < 0)
                    throw new System.Exception("Module of complex number can't be less than 0");
                else
                    mod = value;
            }
        }
        public float Arg
        {
            get
            {
                if (mod != 0)
                    return arg;
                else
                    return 0;
            }
            set
            {
                arg = value;
            }
        }

        public float Re
        {
            get
            {
                return Mod * Mathf.Cos(Arg);
            }
            set
            {
                CalcComplex(value, Im);
            }
        }

        public float Im
        {
            get
            {
                return Mod * Mathf.Sin(Arg);
            }
            set
            {
                CalcComplex(Re, value);
            }
        }

        void CalcComplex(float re,float im)
        {
            if (im == 0 && re == 0)
            {
                Arg = 0;
                Mod = 0;
                return;
            }

            if ( re == 0)
            {
                Arg = Mathf.Sign(im) * Mathf.PI / 2;
            }
            else
            {
                Arg = Mathf.Atan2(Im, Re);
            }
            Mod = re / Mathf.Cos(Arg);
        }

        public static ComplexFloat operator +(ComplexFloat a, ComplexFloat b)
        {
            ComplexFloat complex = new ComplexFloat();
            complex.Re = a.Re + b.Re;
            complex.Im = a.Im + b.Im;
            return complex;
        }
        public static ComplexFloat operator *(ComplexFloat a, ComplexFloat b)
        {
            ComplexFloat complex = new ComplexFloat();
            complex.Mod = a.Re + b.Re;
            complex.Im = a.Im + b.Im;
            return complex;
        }
        public static bool operator ==(ComplexFloat a, ComplexFloat b)
        {
            return a.Arg == b.Arg && a.Mod == b.Mod;
        }
        public static bool operator !=(ComplexFloat a, ComplexFloat b)
        {
            return a.Arg != b.Arg || a.Mod != b.Mod;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj); 
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return Re + " + i" + Im;
        }
    }

    public static partial class MathPlus
    {
        public static ComplexFloat[] GetUnityRoots(uint degree)
        {
            if (degree == 0)
                throw new System.Exception("There no exist 0 degree unity roots");

            ComplexFloat[] roots = new ComplexFloat[degree];
            for(int i =0;i<degree;i++)
            {
                roots[i] = new ComplexFloat();
                roots[i].Mod = 1;
                roots[i].Arg = Mathf.PI * 2 / i;
            }

            return roots;
        }
    }
}