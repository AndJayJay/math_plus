﻿using System;
using System.Collections;
using UnityEngine;

namespace MathPlus.Polynomials
{
    public interface Polynomial<T> : IEnumerator, IEnumerable
    {
        T this[uint i] { get;set; }
        T GetValue(float x);
        int Degree { get; set; }
    }

    public class PolynomialF : Polynomial<float>
    {
        float[] m_Factors;
        int m_EnumeratorIndex = -1;
        public int Degree
        {
            get => m_Factors.Length;
            set
            {
                if (Degree > value)
                    throw new Exception("Can't change polynomial degree. Data lost.");
                else if(Degree < value)
                {
                    float[] temp = new float[value];
                    int count = Degree;
                    for (uint i = 0; i < count; i++)
                        temp[i] = m_Factors[i];
                    m_Factors = temp;
                }
            }
        }

        public PolynomialF() => InitializeFactors(new float[] { default });
        public PolynomialF(int degree) => InitializeFactors(new float[degree]);
        public PolynomialF(params float[] factors) => InitializeFactors(factors);


        #region IEnumerator&IEnumerable implementation

        bool IEnumerator.MoveNext() => ++m_EnumeratorIndex < Degree;

        void IEnumerator.Reset() => m_EnumeratorIndex = 0;

        IEnumerator IEnumerable.GetEnumerator() => this;

        object IEnumerator.Current { get => m_Factors[m_EnumeratorIndex]; }

        #endregion

        void InitializeFactors(float[] factors)
        {
            m_Factors = new float[factors.Length];
            for (int i = 0; i < factors.Length; i++)
                m_Factors[i] = factors[i];
        }

        public float this[uint i]
        {
            get => i >= Degree ? default : m_Factors[i];
            set => m_Factors[i] = i < Degree ? value : throw new Exception("Index " + i + ", out of range.");
        }

        public float GetValue(float x)
        {
            float powerX = 1;
            float value = this[0];

            foreach (float factor in this)
            {
                value += factor * powerX;
                powerX *= x;
            }
            return value;
        }

        public static PolynomialF operator +(PolynomialF a, PolynomialF b)
        {
            PolynomialF poly = new PolynomialF(a.Degree > b.Degree ? a.Degree : b.Degree);

            for(uint i=0;i<poly.Degree;i++)
            {
                if (i < a.Degree)
                    poly[i] += a[i];
                if (i < b.Degree)
                    poly[i] += b[i];
            }
            
            return poly;
        }

        public static PolynomialF operator -(PolynomialF a, PolynomialF b)
        {
            PolynomialF poly = new PolynomialF(a.Degree > b.Degree ? a.Degree : b.Degree);

            for (uint i = 0; i < poly.Degree; i++)
            {
                if (i < a.Degree)
                    poly[i] += a[i];
                if (i < b.Degree)
                    poly[i] -= b[i];
            }

            return poly;
        }

        public static PolynomialF operator *(PolynomialF a, PolynomialF b)
        {
            PolynomialF poly = new PolynomialF(a.Degree + b.Degree-1);
            PolynomialF poly1 = a.Degree > b.Degree ? a : b;
            PolynomialF poly2 = a.Degree > b.Degree ? b : a;

            for (uint i = 0; i < poly.Degree; i++)
            {
                for (uint j = 0; j < poly1.Degree; j++)
                {
                    if (poly2.Degree > i-j)
                        if (i - j >= 0)
                            poly[i] += poly1[j] * poly2[i - j];
                        else
                            break;
                }
            }
            return poly;
        }

        public override bool Equals(object obj)
        {
            int count1, count2;
            PolynomialF temp1 = obj as PolynomialF;
            if(temp1 != null)
            {
                PolynomialF temp2 = Degree> temp1.Degree?this: temp1;
                temp1 = Degree > temp1.Degree ? temp1 : this;
                count1 = temp1.Degree;
                count2 = temp2.Degree;
                for(uint i = 0;i< count2; i++)
                {
                    if (i > count1 && temp2[i] != 0)
                        return false;
                    else if (temp2[i] != temp1[i])
                        return false;
                }
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            string temp = "";
            int count = Degree;
            for (int i = count-1; i >= 0; i--)
            {
                temp += this[(uint)i];
                if (i > 0)
                    temp += "x^" + i+"+";
            }
            return temp;
        }
    }

    public class PolynomialV3 : Polynomial<Vector3>
    {
        PolynomialF[] m_Factors = new PolynomialF[3];
        int m_EnumeratorIndex = -1;
        public int Degree
        {
            get => m_Factors[0].Degree;
            set
            {
                if (Degree > value)
                    throw new Exception("Can't change polynomial degree. Data lost.");
                else if (Degree < value)
                {
                    m_Factors[0].Degree = value;
                    m_Factors[1].Degree = value;
                    m_Factors[2].Degree = value;
                }
            }
        }

        public PolynomialV3() => InitializeFactors(new Vector3[] { default });
        public PolynomialV3(int degree) => InitializeFactors(new Vector3[degree]);
        public PolynomialV3(params Vector3[] factors) => InitializeFactors(factors);


        #region IEnumerator&IEnumerable implementation

        bool IEnumerator.MoveNext() => ++m_EnumeratorIndex < Degree;

        void IEnumerator.Reset() => m_EnumeratorIndex = 0;

        IEnumerator IEnumerable.GetEnumerator() => this;

        object IEnumerator.Current { get => m_Factors[m_EnumeratorIndex]; }

        #endregion

        void InitializeFactors(Vector3[] factors)
        {
            m_Factors = new PolynomialF[3];
            m_Factors[0] = new PolynomialF(factors.Length);
            m_Factors[1] = new PolynomialF(factors.Length); 
            m_Factors[2] = new PolynomialF(factors.Length);

            for (uint i = 0; i < factors.Length; i++)
            {
                m_Factors[0][i] = factors[i].x;
                m_Factors[1][i] = factors[i].y;
                m_Factors[2][i] = factors[i].z;
            }
        }

        public Vector3 this[uint i]
        {
            get => i >= m_Factors.Length ? default : new Vector3(m_Factors[0][i], m_Factors[i][i], m_Factors[2][i]);
            set
            {
                if (i >= m_Factors.Length)
                    throw new Exception("Index " + i + ", out of range.");
                else 
                {
                    m_Factors[0][i] = value.x;
                    m_Factors[1][i] = value.y;
                    m_Factors[2][i] = value.z;

                }
            }
        }

        public PolynomialF GetFactorsX() => m_Factors[0];
        public PolynomialF GetFactorsY() => m_Factors[1];
        public PolynomialF GetFactorsZ() => m_Factors[2];
        public void SetFactorsX(PolynomialF factors)
        {
            m_Factors[0] = factors;
            if (m_Factors[0].Degree > m_Factors[1].Degree)
            {
                m_Factors[1].Degree = m_Factors[0].Degree;
                m_Factors[2].Degree = m_Factors[0].Degree;
            }
            else if (m_Factors[0].Degree < m_Factors[1].Degree)
                m_Factors[0].Degree = m_Factors[1].Degree;
        }
        public void SetFactorsY(PolynomialF factors)
        {
            m_Factors[1] = factors;
            if (m_Factors[1].Degree > m_Factors[2].Degree)
            {
                m_Factors[0].Degree = m_Factors[1].Degree;
                m_Factors[2].Degree = m_Factors[1].Degree;
            }
            else if (m_Factors[1].Degree < m_Factors[2].Degree)
                m_Factors[1].Degree = m_Factors[2].Degree;
        }
        public void SetFactorsZ(PolynomialF factors)
        {
            m_Factors[2] = factors;
            if (m_Factors[2].Degree > m_Factors[0].Degree)
            {
                m_Factors[0].Degree = m_Factors[2].Degree;
                m_Factors[1].Degree = m_Factors[2].Degree;
            }
            else if (m_Factors[2].Degree < m_Factors[0].Degree)
                m_Factors[2].Degree = m_Factors[0].Degree;
        }

        public Vector3 GetValue(float x)
        {
            return new Vector3(m_Factors[0].GetValue(x), m_Factors[1].GetValue(x), m_Factors[2].GetValue(x));
        }

        public static PolynomialV3 operator +(PolynomialV3 a, PolynomialV3 b)
        {
            PolynomialV3 poly = new PolynomialV3(a.Degree > b.Degree ? a.Degree : b.Degree);
            poly.SetFactorsX(a.GetFactorsX() + b.GetFactorsX());
            poly.SetFactorsY(a.GetFactorsY() + b.GetFactorsY());
            poly.SetFactorsZ(a.GetFactorsZ() + b.GetFactorsZ());
            return poly;
        }

        public static PolynomialV3 operator -(PolynomialV3 a, PolynomialV3 b)
        {
            PolynomialV3 poly = new PolynomialV3(a.Degree > b.Degree ? a.Degree : b.Degree);
            poly.SetFactorsX(a.GetFactorsX() - b.GetFactorsX());
            poly.SetFactorsY(a.GetFactorsY() - b.GetFactorsY());
            poly.SetFactorsZ(a.GetFactorsZ() - b.GetFactorsZ());
            return poly;
        }

        public static PolynomialV3 operator *(PolynomialV3 a, PolynomialV3 b)
        {
            PolynomialV3 poly = new PolynomialV3(a.Degree + b.Degree-1);
            poly.SetFactorsX(a.GetFactorsX() * b.GetFactorsX());
            poly.SetFactorsY(a.GetFactorsY() * b.GetFactorsY());
            poly.SetFactorsZ(a.GetFactorsZ() * b.GetFactorsZ());
            return poly;
        }

        public override bool Equals(object obj)
        {
            PolynomialV3 temp = obj as PolynomialV3;
            return temp!=null 
                && GetFactorsX().Equals(temp.GetFactorsX()) 
                && GetFactorsY().Equals(temp.GetFactorsY()) 
                && GetFactorsZ().Equals(temp.GetFactorsZ());
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            string temp = "";
            int count = Degree;
            for (int i = count - 1; i >= 0; i--)
            {
                temp += this[(uint)i];
                if (i > 0)
                    temp += "x^" + i + "+";
            }
            return temp;
        }

        //public 
    }
}